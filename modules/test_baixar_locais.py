#!/usr/bin/env python
# coding: utf8
import sys
sys.path.append('../../..')
from baixar_locais import BaixarLocais

locais = {
    'Zona Norte' : ('São Cristovão','Cascadura','Madureira'),
    'Zona Sul' : ('Botafogo','Flamengo','Ipanema'),
    'Zona Oeste' : ('Barra da Tijuca','Recreio dos Bandeirantes','Jacarepaguá'),
    'Baixada Fluminense' : ('Duque de Caxias','Nilópolis'),
}



class dbMock():
    _locais_no_banco = []
    
    def tabela_locais_no_banco(self):
        locais = self._locais_no_banco
        self._locais_no_banco = []
        return locais
        
    def commit(self):
        pass
        
    def __init__(self):
        
        class locais():
            def __init__(self,superclasse):
                self.superclasse  = superclasse
                
            def insert(self,**kwargs):
                self.superclasse._locais_no_banco.append(kwargs)
                
        self.locais = locais(self)
    
class requestMock():
    pass

class responseMock():
    pass
    
bl = BaixarLocais(dbMock(),requestMock(),responseMock())

def test_deveria_inserir_zona_sul():
    bl.local_por_nome = lambda nome: None
    bl('''---
    nome: Zona Sul
    local_pai: ''')
    
    assert [{'local_pai': None, 'nome': 'Zona Sul', 
        'latitude': -22.9836628, 'longitude': -43.1964029}] == bl.db.tabela_locais_no_banco()
    
def test_deveria_inserir_botafogo_e_zona_sul():
    bl.local_por_nome = lambda nome: 1 if nome == 'Zona Sul' else None
    bl('''
    ---
    nome: Zona Sul
    local_pai: 
    ---
    nome: Botafogo
    local_pai: Zona Sul
    '''.replace('    ',''))
    
    assert [
        {'local_pai': None, 'nome': 'Zona Sul', 'latitude': -22.9836628, 'longitude': -43.1964029},
        {'local_pai': 1, 'nome': 'Botafogo', 'latitude': -22.9463512, 'longitude': -43.1828052}
    ] == bl.db.tabela_locais_no_banco()
