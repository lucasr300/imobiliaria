#!/usr/bin/env python
# coding: utf8
from gluon.tools import geocode
import yaml

class BaixarLocais():
    def __init__(self,db,request,response):
	self.db, self.request, self.response = (db,request,response)
	
    def obter_locais(self,locais_yaml):
	return tuple(yaml.load_all(locais_yaml))
	
    def local_por_nome(self,nome):
	return self.db.locais(nome=nome).select()[0]
    
    def __call__(self,locais_yaml):
	for local in self.obter_locais(locais_yaml):
	    nome = local['nome']
	    longitude, latitude = geocode('%s, Rio de Janeiro, Brasil' % nome)
	    local_pai = local.get('local_pai')
	    
	    if local_pai:
		local_pai= self.local_por_nome(local_pai)
	    
	    self.db.locais.insert(
		nome = nome,
		local_pai = local_pai,
		latitude = latitude,
		longitude = longitude,
	    )
	    
	self.db.commit()

	
