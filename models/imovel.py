# -*- coding: utf-8 -*-

from PIL import Image
import os

_imoveis = db.define_table('imoveis',
    Field('titulo','string'),
    Field('slug','string'),
    Field('localizacao','string'),
    Field('descricao','text'),
    Field('local','reference locais'),
    Field('preco','double'),
)

_imovel = lambda query: _imoveis(query)

_fotos_imoveis = db.define_table('fotos_imoveis',
    Field('foto','upload'),
    Field('imovel','reference imoveis'),
    Field('legenda','text'),
)
_foto_imovel = lambda query: _fotos_imoveis(query)
_fotos_do_imovel = lambda id: _fotos_imoveis(imovel=id)


class ImoveisModel(object):
    def url(self):
        return URL(request.application,'imovel','index',args=[self.imoveis.id,self.imoveis.slug])

    def url_contato(self):
        return URL(request.application,'imovel','contato',args=[self.imoveis.id])

    def preco_formatado(self):
        preco = self.imoveis.preco
        if not preco:
            return 'Valor sob consulta'

        preco = str(int(preco))
        preco = preco[:-3] + '.' + preco[-3:]
        return 'R$ %s' % preco

    def foto_principal(self):
        fotos = db(db.fotos_imoveis.imovel==self.imoveis.id).select()
        return fotos[0] if fotos else None

_imoveis.virtualfields.append(ImoveisModel())

class FotosImoveisModel(object):
    pasta_das_fotos_estaticas = 'static/images/'
    
    @staticmethod
    def _colocar_tamanho_antes_da_extensao(caminho):
        caminho, extensao = os.path.splitext(caminho)
        return '%s_%s%s' % (caminho,'thumb_160',extensao)
        
    def caminho_absoluto(self):
        foto = self.fotos_imoveis.foto
        foto = foto.replace('static:',self.pasta_das_fotos_estaticas)
        return request.folder + 'uploads/' + foto
    
    def url(self):
        # TODO: Verificar url da foto
        foto = self.fotos_imoveis.foto
        if foto.startswith('static:'):
            return foto.replace('static:',
                '/%s/%s' % (request.application,self.pasta_das_fotos_estaticas))

        return URL(request.application,'default','download',args=['db',foto])

    def url_miniatura(self):
        self.gerar_miniatura()
        return self._colocar_tamanho_antes_da_extensao(self.url())
    
    def gerar_miniatura(self,forcar=False):
        caminho_miniatura = self._colocar_tamanho_antes_da_extensao(self.caminho_absoluto())
        
        if not forcar and os.path.isfile(caminho_miniatura):
            return False

        image = Image.open(self.caminho_absoluto())
        image.thumbnail((160, 120), Image.ANTIALIAS)
        image.save(caminho_miniatura)
        
        return True

_fotos_imoveis.virtualfields.append(FotosImoveisModel())

_msgs_contato = db.define_table('msgs_contato',
    Field('nome','string',requires=IS_NOT_EMPTY()),
    Field('email','string',requires=IS_EMAIL()),
    Field('imovel',db.imoveis),
    Field('telefone','string'),
    Field('mensagem','text'),
)
