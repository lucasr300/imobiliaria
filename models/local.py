# -*- coding: utf-8 -*-
from math import sqrt, radians
from gluon.dal import lazy_virtualfield

_locais = db.define_table('locais',
    Field('nome','string'),
    Field('slug','string'),
    Field('local_pai','reference locais'),
    Field('latitude','double'),
    Field('longitude','double'),
)

class LocaisModel(object):
    def url(self):
        return URL(request.application,'locais','index',args=[self.locais.slug])

    def nome_preposicao(self):
        slug = self.locais.slug or self.locais.nome.lower().replace(' ','-')

        sufixos = dict(
            em = ' ama ema ima uma ana ena ina una',
            no = ' ngo on er te io ro ai au do il me mbi',
            na = ' ca ua ta ia la ina nha na ma',
        )

        prefixos = dict(
            em = ' são santo santa',
            no = ' jardim parque largo engenho rio',
            na = ' zona vila ilha barra cidade',
        )

        preposicao_correta = 'em'

        for preposicao, lista_sufixos in sufixos.iteritems():
            lista_sufixos = lista_sufixos.split()
            for sufixo in lista_sufixos:
                if slug.endswith(sufixo):
                    preposicao_correta = preposicao

        for preposicao, lista_prefixos in prefixos.iteritems():
            lista_prefixos = lista_prefixos.split()
            for prefixo in lista_prefixos:
                if slug.startswith(prefixo):
                    preposicao_correta = preposicao

        return '%s %s' % (preposicao_correta,self.locais.nome)

    @lazy_virtualfield
    def locais_filhos(instance,row):
        return db(_locais.local_pai==row.locais).select()

    @lazy_virtualfield
    def locais_filhos_por_inferencia(instance,row):
        locais = []
        for local in row.locais.locais_filhos():
            locais.append(local)
            locais.extend(local.locais_filhos_por_inferencia())
        return locais

    @lazy_virtualfield
    def distancia_ate(instance,row,outro_local):
        este_local = row.locais

        este_local_latitude = radians(este_local.latitude)
        este_local_longitude = radians(este_local.longitude)
        outro_local_latitude = radians(outro_local.latitude)
        outro_local_longitude = radians(outro_local.longitude)

        return sqrt((outro_local_longitude - este_local_longitude)**2 + (outro_local_latitude - este_local_latitude)**2)

    @lazy_virtualfield
    def e_proximo_a(instance,row,outro_local):
        este_local = row.locais
        return int(este_local.distancia_ate(outro_local) * 1000) < 3

    @lazy_virtualfield
    def locais_proximos(instance,row):
        este_local = row.locais
        if not este_local.latitude or not este_local.longitude: return []

        latitude_minima = este_local.latitude - 1
        latitude_maxima = este_local.latitude + 1
        longitude_minima = este_local.longitude - 1
        longitude_maxima = este_local.longitude + 1

        locais_possivelmente_proximos = db((db.locais.latitude < latitude_maxima) & \
                                        (db.locais.latitude > latitude_minima) & \
                                        (db.locais.longitude < longitude_maxima) & \
                                        (db.locais.longitude > longitude_minima) & \
                                        (db.locais.id != este_local.id)).select()

        locais_proximos = filter(lambda local: local.e_proximo_a(este_local), locais_possivelmente_proximos)
        locais_proximos.sort(lambda x,y: -1 if x.distancia_ate(este_local) < y.distancia_ate(este_local) else 1)
        return locais_proximos

_local = lambda query: _locais(query)
_locais.virtualfields.append(LocaisModel())

