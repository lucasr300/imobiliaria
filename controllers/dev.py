# coding: utf8

from random import choice, randint

# ##########################################################
# ## fixtures
# ###########################################################

remote_addr = request.env.remote_addr
if remote_addr != "127.0.0.1":
    raise HTTP(403, '')

def fixtures():
    nome_da_fixture = request.args(0) or 'nada'

    if nome_da_fixture == 'drop':
        map(lambda table: table.drop(), [getattr(db,table) for table in db.tables])

    if nome_da_fixture == '20_imoveis':

            zona_norte = _local(_locais.insert(nome='Zona Norte',slug='zona-norte'))
            zona_oeste = _local(_locais.insert(nome='Zona Oeste',slug='zona-oeste'))

            locais = [
                _local(_locais.insert(nome='Tijuca',slug='tijuca',local_pai=zona_norte.id,latitude=-21.14,longitude=-43.15)),
                _local(_locais.insert(nome='Barra',slug='barra-da-tijuca',local_pai=zona_oeste.id,latitude=-21.18,longitude=-43.16)),
                _local(_locais.insert(nome='Cidade Nova',slug='cidade-nova',latitude=-21.13,longitude=-43.14)),
                _local(_locais.insert(nome='São Cristovão',slug='sao-cristovao',local_pai=zona_norte.id,latitude=-21.14,longitude=-43.14)),
                _local(_locais.insert(nome='Recreio',slug='recreio',local_pai=zona_oeste.id,latitude=-21.18,longitude=-43.15)),
                _local(_locais.insert(nome='Cascadura',slug='cascadura',local_pai=zona_norte.id,latitude=-21.16,longitude=-43.15)),
            ]

            for i in range(20):
                tipo = choice(['Apartamento','Casa Padrão','Casa de Vila'])
                local = choice(locais)
                localizacao = choice(['Próximo a estrada','Ao lado do supermercado','A duas quadras do shopping'])
                titulo = "%s %s" % (tipo,local.nome_preposicao)

                id = _imoveis.insert(titulo=titulo,
                    localizacao = localizacao,
                    preco = randint(40,400) * 1000,
                    local = local,
                    slug = titulo.lower().replace(' ','-'),
                    descricao='%s %s, com %s, %s. %s. %s.' %(
                        choice(['Belissímo imovel','Oferta imperdível','Lançamento']),
                        local.nome_preposicao,
                        choice(['1 quarto','2 quartos','4 quartos e 2 suítes']),
                        choice(['sala, cozinha e banheiro','amplo salão e piscina','copa-cozinha e sacada']),
                        choice(['Sol da manhã','Vista para o mar','Rua tranquila']),
                        choice(['Aceito carta','Somente a Vista','Financiado']),
                    )
                )

                for i in range(randint(1,3)):
                    _fotos_imoveis.insert(foto = 'static:fixtures/foto_%s.jpg' % randint(1,9), imovel = id)

    return dict(message="%s foi executado." % nome_da_fixture)
