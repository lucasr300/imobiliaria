# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

def _test():
    '''
    >>> from PIL import Image
    >>> import os


    deveria criar
    >>> id = _imoveis.insert(id=1,titulo='Casa Padrao',slug='casa-padrao')
    >>> imovel = _imoveis(id)

    deveria ter titulo
    >>> imovel.titulo
    'Casa Padrao'

    deveria ter preco
    >>> imovel2 = _imovel(_imoveis.insert(id=2,preco=25000.00))
    >>> imovel2.preco_formatado
    'R$ 25.000'

    deveria retornar mensagem caso não tenha preco
    >>> imovel.preco_formatado
    'Valor sob consulta'

    deveria ter local
    >>> barra_da_tijuca = _locais.insert(nome='Barra da Tijuca')
    >>> imovel.local = barra_da_tijuca
    >>> imovel.update_record()
    >>> imovel.local.nome
    'Barra da Tijuca'

    imovel deveria ter url principal
    >>> imovel.url
    '/1/casa-padrao.html'

    e url para formulário de contato
    >>> imovel.url_contato
    '/1/entre-em-contato.html'

    deveria ter tabela de fotos
    >>> foto = _fotos_imoveis.insert(foto='static:fixtures/foto_1.jpg',imovel=1)

    foto deveria ter url
    >>> foto = _foto_imovel(foto)
    >>> foto.url
    '/imoveis/static/images/fixtures/foto_1.jpg'

    o caminho absoluto da imagem (apenas para uso interno)
    >>> esperado = '/web2py/applications/%s/static/images/fixtures/foto_1.jpg' % request.application
    >>> caminho_absoluto = _imovel(1).foto_principal.caminho_absoluto
    >>> caminho_absoluto.endswith(esperado)
    True

    deveria retornar url da foto principal
    >>> _imovel(1).foto_principal.url
    '/imoveis/static/images/fixtures/foto_1.jpg'

    e tambem a url da miniatura (que vai chamar o metodo que regera a imagem)
    >>> _imovel(1).foto_principal.url_miniatura
    '/imoveis/static/images/fixtures/foto_1_thumb_160.jpg'

    apenas para o teste, vamos excluir o arquivo gerado pelo metodo url_miniatura
    (e garantir que ele existe)
    >>> caminho_absoluto_da_miniatura = caminho_absoluto.replace('.jpg','_thumb_160.jpg')
    >>> os.remove(caminho_absoluto_da_miniatura)

    regerar a miniatura
    >>> _imovel(1).foto_principal.gerar_miniatura
    True

    e tentar acessar o arquivo com o PIL (para ver se a imagem e valida 
    e tem o tamanho correto)
    >>> Image.open(caminho_absoluto_da_miniatura).size
    (160, 71)
    
    Ao chamar o metodo_gerar_miniatura novamente, ele deve retornar False
    indicando que o arquivo ja existia
    >>> _imovel(1).foto_principal.gerar_miniatura
    False

    excluindo novamente o arquivo gerado
    >>> os.remove(caminho_absoluto_da_miniatura)
    
    
    deveria excluir foto
    >>> _ = foto.delete_record()

    nao deveria retornar foto principal caso o imovel não tenha foto
    >>> _imovel(1).foto_principal

    deveria excluir imovel
    >>> _ = imovel.delete_record()
    >>> _ = imovel2.delete_record()
    >>> _ = barra_da_tijuca.delete_record()
    '''
    pass

def index():
    '''
    >>> imovel = _imovel(_imoveis.insert(titulo='casa'))
    >>> foto = _fotos_imoveis.insert(foto='static:fixtures/foto_1.jpg',imovel=imovel)

    deveria retornar imovel
    >>> request.args.append(imovel.id)
    >>> index()['imovel'].titulo
    'casa'

    deveria ter foto e poder ter mais de uma
    >>> fotos = index()['fotos']
    >>> len(fotos)
    1
    >>> fotos[0]['url']
    '/imoveis/static/images/fixtures/foto_1.jpg'

    >>> _ = request.args.pop()
    >>> _= foto.delete_record()
    >>> _= imovel.delete_record
    '''
    # TODO: testar 404
    # TODO: formato preco

    id = request.args(0)
    imovel = _imoveis(id)

    if not imovel: raise HTTP(404)

    fotos =  db(_fotos_imoveis.imovel==imovel).select().as_list()
    response.title = imovel.titulo
    return dict(imovel=imovel,fotos=fotos)

def contato():
    id = request.args(0)
    imovel = _imoveis(id)
    if not imovel: raise HTTP(404)

    fields = [field.name for field in _msgs_contato]
    fields.remove('imovel')
    form = SQLFORM(_msgs_contato,fields=fields,submit_button='Registrar interesse',
        labels=dict(mensagem='Outras informações',email='E-mail'))

    if form.accepts(request):
        _msgs_contato(form.vars.id).update_record(imovel=imovel)
        response.flash = 'Sua mensagem foi enviada'
        
    if form.errors:
        response.flash = 'Corrija os campos indicados'
        response.status = 409

    response.title = 'Entre em contato'
    return dict(form=form)
    


    
