# coding: utf8

def _test():
    '''
    deveria criar sem hierarquia
    >>> tijuca = _locais.insert(nome='Tijuca',latitude=-22.123456,longitude=-43.123456)
    >>> madureira = _locais.insert(nome='Madureira',latitude=-22.000,longitude=-43.000)

    deveria retornar as coordenadas com as casas decimais
    >>> tijuca.latitude
    -22.123456
    >>> tijuca.longitude
    -43.123456

    deveria ter locais filhos
    >>> andarai = _locais.insert(nome='Andarai',local_pai=tijuca,latitude=-22.123400,longitude=-43.123400)
    >>> andarai.local_pai.nome
    'Tijuca'

    deveria ter local pai
    >>> zona_norte = _locais.insert(nome='Zona Norte',slug='zona-norte')
    >>> tijuca.local_pai = zona_norte
    >>> tijuca.update_record()
    >>> tijuca.local_pai.nome
    'Zona Norte'

    deveria ter locais filhos
    >>> [ local.nome for local in zona_norte.locais_filhos()]
    ['Tijuca']

    deveria ter url
    >>> zona_norte.url
    '/zona-norte.html'

    andarai tambem deve pertencer a zona norte
    >>> _local(andarai.local_pai).local_pai.nome
    'Zona Norte'

    deveria criar com hierarquia
    >>> grajau =_locais.insert(nome='Grajau',local_pai=tijuca,latitude=-22.123299,longitude=-43.123299)
    >>> grajau.local_pai.nome
    'Tijuca'

    ao criar um local sem especificar as coordenadas, elas devem ser None por padrao
    afinal, onde fica Tangamandapio?
    >>> tangamandapio = _locais.insert(slug='tangamandapio')
    >>> tangamandapio.latitude, tangamandapio.longitude
    (None, None)

    locais filhos por inferencia, deveria trazer os filhos e os netos dos locais
    >>> [ local.nome for local in zona_norte.locais_filhos_por_inferencia()]
    ['Tijuca', 'Andarai', 'Grajau']

    distancia inteira entre andarai e madureira é de 3 kud
    >>> int(andarai.distancia_ate(madureira) * 1000)
    3

    distancia entre madureira e andarai e a mesma entre andarai e madureira
    >>> madureira.distancia_ate(andarai) == andarai.distancia_ate(madureira)
    True

    tijuca fica mais perto do andarai do que o grajau
    >>> tijuca.distancia_ate(andarai) < grajau.distancia_ate(andarai)
    True

    madureira fica longe do andarai
    >>> madureira.e_proximo_a(andarai)
    False

    e perto da tijuca
    >>> andarai.e_proximo_a(tijuca)
    True

    e do grajau
    >>> andarai.e_proximo_a(grajau)
    True

    e proximo a ele mesmo
    >>> andarai.e_proximo_a(andarai)
    True

    tijuca e andarai são locais proximos a grajau; madureira não;
    esta lista deve estar odernada por proximidade
    >>> [ local.nome for local in grajau.locais_proximos() ]
    ['Andarai', 'Tijuca']

    tangamandapio não fica perto de nada
    >>> len(tangamandapio.locais_proximos())
    0

    preposicao antes do nome
    >>> zona_norte.nome_preposicao, tijuca.nome_preposicao,
    ('na Zona Norte', 'na Tijuca')

    >>> andarai.nome_preposicao, grajau.nome_preposicao,
    ('no Andarai', 'no Grajau')

    >>> madureira.nome_preposicao
    'em Madureira'

    deveria excluir
    >>> _ = zona_norte.delete_record()
    >>> _ = andarai.delete_record()
    >>> _ = tijuca.delete_record()
    >>> _ = madureira.delete_record()
    >>> _ = grajau.delete_record()
    >>> _ = tangamandapio.delete_record()
    '''

def index():
    '''
    >>> barra_da_tijuca = _locais.insert(nome='Barra da Tijuca',slug='barra-da-tijuca')
    >>> casa = _imoveis.insert(slug='casa',local=barra_da_tijuca)

    deveria retornar o proprio local e os imoveis associados a ele
    >>> request.args.append('barra-da-tijuca')
    >>> index()['local'].nome
    'Barra da Tijuca'
    >>> index()['imoveis'].__len__()
    1
    >>> index()['imoveis'][0].slug
    'casa'

    >>> _ = request.args.pop()
    >>> _ = casa.delete_record()
    >>> _ = barra_da_tijuca.delete_record()

    deveria exibir os imoveis nos locais imediatamente inferiores;
    ao listar a Zona Norte, deveria trazer tambem os imoveis de
    Madureira e Cascadura, alem da propria Zona Norte;
    ordenado por preco, do menor para o maior

    >>> zona_norte = _locais.insert(slug='zona-norte')
    >>> madureira = _locais.insert(slug='madureira',local_pai=zona_norte)
    >>> cascadura = _locais.insert(slug='cascadura',local_pai=zona_norte)

    >>> _ = _imoveis.insert(local=zona_norte,slug='casa-zn',preco=20000.0)
    >>> _ = _imoveis.insert(local=madureira,slug='apto-m',preco=30000.0)
    >>> _ = _imoveis.insert(local=cascadura,slug='loja-c',preco=10000.0)

    >>> request.args.append('zona-norte')
    >>> index()['imoveis'].__len__()
    3
    >>> index()['imoveis'][0].slug, index()['imoveis'][1].slug, index()['imoveis'][2].slug
    ('loja-c', 'casa-zn', 'apto-m')

    >>> _ = [registro.delete_record() for registro in db(_imoveis).select()]
    >>> _ = [registro.delete_record() for registro in db(_locais).select()]

    '''

    local = db(_locais.slug==request.args(0)).select()
    if not local: raise HTTP(404)
    local = local[0]

    imoveis = list(db(_imoveis.local==local).select())
    for local_filho in local.locais_filhos_por_inferencia():
        imoveis_no_local_filho = db(_imoveis.local==local_filho).select()
        imoveis.extend(list(imoveis_no_local_filho))

    imoveis.sort(key=lambda imovel: imovel.preco)
    response.title = 'Imóveis a venda %s' % local.nome_preposicao
    return dict(local=local,imoveis=imoveis)
